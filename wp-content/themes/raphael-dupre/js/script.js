(function($){

    // Le chargement du DOM est terminé
    jQuery(document).ready(function() {  

        $('a[href^="#"]').click(function(){
            var id = $(this).attr("href");
            if (id === '#') {
                return;
            }
            $('html, body').animate({scrollTop:$(id).offset().top - 40}, 'slow');
        });

    });

    jQuery(window).load(function() {
  
        //

    });
  
})(jQuery);