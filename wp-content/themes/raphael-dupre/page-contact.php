<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section id="primary" class="content-area col-md-12">
		<div id="main" class="site-main" role="main">

			<!-- Formulaire de contact -->
			<div class="contact-page-wrapper">
				<div class="container">
					<div class="contact-page row">

						<?php

							$titre_contact = get_field( "titre_page_contact" );
							$description_contact = get_field( "description_page_contact" );
							$shortcode_contact = get_field( "code_contact_form" );

							echo '<div class="contact-page-left col-md-6">';
								echo '<h1 class="titre-design">'.$titre_contact.'</h1>';
								echo $description_contact;
								
						?>

								<!-- Mes réseaux -->
								<div class="contact-page-rs-sup">

									<a href="https://www.malt.fr/profile/raphaeldupre">
										<img src="https://www.raphael-dupre.fr/wp-content/themes/raphael-dupre/img/malt.png" alt="Malt" draggable="false">
									</a>
									<a href="https://fr.linkedin.com/in/rapha%C3%ABl-dupr%C3%A9-42859bb1" target="_blank">
										<img src="https://www.raphael-dupre.fr/wp-content/themes/raphael-dupre/img/linkedin.png" alt="Linkedin" draggable="false">
									</a>

								</div>

						<?php 
							
							echo '</div>';
							echo '<div class="contact-page-right col-md-6">';
								echo do_shortcode($shortcode_contact);
							echo '</div>';

						?>

					</div>
				</div>
			</div>

		</div><!-- #main -->
	</section><!-- #primary -->

<?php
get_sidebar();
get_footer();
