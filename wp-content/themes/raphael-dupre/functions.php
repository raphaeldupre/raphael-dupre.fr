<?php
/**
** wp-bootstrap-starter
**/
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
 wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

}

function raphael_dupre_custom_new_menu() {
    register_nav_menu('secondary-menu',__( 'Menu secondaire' ));
  }
  add_action( 'init', 'raphael_dupre_custom_new_menu' );

?>