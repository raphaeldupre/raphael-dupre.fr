<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

	<section id="primary" class="content-area col-md-12">
		<div id="main" class="site-main" role="main">

			<!-- Header Top -->
			<div class="page-header-top-wrapper">
				<div class="container">
					<div class="page-header-top row">
						<div class="page-header-left col-md-7 order-md-1 order-2">
	
							<?php
	
								$np_header = get_field( "header_nom__prenom" );
								$profession_header = get_field( "header_profession" );
								$st_header = get_field( "header_sous-titre" );
								$lien_header = get_field( "header_lien" );
								
								echo '<h2>'.$np_header.'</h2>';
								echo '<h1>'.$profession_header.'</h1>';
								echo '<h3>'.$st_header.'</h3>';
								echo '<div class="global-btn-jaune">';
									echo '<a href="'.esc_url($lien_header['url']).'" class="btn-jaune">'.esc_html($lien_header['title']).'</a>';
								echo '</div>';
	
							?>
							
						</div>
						<div class="page-header-right col-md-5 order-md-2 order-1">
	
							<?php
	
								$image_header = get_field( "header_image" );
	
								echo '<img src="'.esc_url($image_header['url']).'" alt="'.esc_attr($image_header['alt']).'" draggable="false" />';
							
							?>
						</div>
					</div>
				</div>
			</div>

			<!-- Qui suis-je ? -->
			<div class="qui-suis-je-home-wrapper" id="qui-suis-je">
				<div class="container">
					<div class="qui-suis-je-home row">
						<div class="qui-suis-je-left col-md-5">
	
							<?php
	
								$titre_qui_suis_je = get_field( "titre_qui_suis-je" );
								
								echo '<h2 class="titre-design">'.$titre_qui_suis_je.'</h2>';
	
							?>
							
						</div>
						<div class="qui-suis-je-right col-md-7">
	
							<?php
	
								$description_qui_suis_je = get_field( "description_qui_suis-je" );
	
								echo $description_qui_suis_je;
							
							?>

						</div>
					</div>
				</div>
			</div>

			<!-- Prestations -->
			<div class="prestations-home-wrapper" id="prestations">
				<div class="container">
					<div class="prestations-home row">
						
						<?php

							$titre_prestation = get_field( "prestation_titre" );
							$description_prestation = get_field( "prestation_description" );

							echo '<div class="prestations-home-titre col-md-12">';
								echo '<h2 class="titre-design">'.$titre_prestation.'</h2>';
								// echo '<div class="col-md-8 offset-md-2">';
								echo $description_prestation;
								// echo '</div>';
							echo '</div>';

							$args = array(  
								'post_type' => 'prestations',
								'post_status' => 'private',
								'posts_per_page' => -1,
								'orderby' => 'date',
								'order' => 'ASC'
							);

							$loop = new WP_Query( $args );
								
							while ( $loop->have_posts() ) : $loop->the_post(); 

								$picto_prestation = get_field( "picto_prestation" );
								$titre_prestation = get_field( "titre_prestation" );
								$description_prestation = get_field( "description_prestation" );
								$lien_prestation = get_field( "lien_prestation" );
								
								echo '<div class="prestations-detail col-md-4 col-sm-6 col-xs-6">';
									echo '<div clas="picto-prestations">';
										echo '<img src="'.esc_url($picto_prestation['url']).'" alt="'.esc_attr($picto_prestation['alt']).'" draggable="false" />';
									echo '</div>';
									echo '<h3>'.$titre_prestation.'</h3>';
									echo $description_prestation;
									// echo '<div class="global-btn-jaune col-md-12">';
									// 	echo '<a href="'.esc_url($lien_prestation['url']).'" target="'.esc_attr($lien_prestation['target']).'" class="btn-jaune">'.esc_html($lien_prestation['title']).'</a>';
									// echo '</div>';
								echo '</div>';

							endwhile;

							wp_reset_postdata();

						?>

						<div class="global-btn-jaune col-md-12">
							<a href="/contact" class="btn-jaune">Me contacter</a>
						</div>

					</div>
				</div>
			</div>

			<!-- Divider -->
			<!-- <div class="divider-element-wrapper">
				<div class="container">
					<div class="divider-element row">
						<div class="global-btn-jaune col-md-12">
							<a href="/contact" class="btn-jaune">Me contacter</a>
						</div>
					</div>
				</div>
			</div> -->
			<!-- <div class="divider-element-wrapper">
			</div> -->

			<!-- Réalisations -->
			<!-- <div class="realisations-home-wrapper" id="realisations">
				<div class="container">
					<div class="realisations-home row">
						
						<?php 

							// $titre_realisation = get_field( "real_titre" );
							// $description_realisation = get_field( "real_description" );
							
							// echo '<div class="realisations-home-titre col-md-12">';
							// 	echo '<h2 class="titre-design">'.$titre_realisation.'</h2>';
							// 	// echo '<div class="col-md-8 offset-md-2">';
							// 	echo $description_realisation;
							// 	// echo '</div>';
							// echo '</div>';

							// $args = array(  
							// 	'post_type' => 'realisations',
							// 	'post_status' => 'private',
							// 	'posts_per_page' => -1,
							// 	'orderby' => 'date',
							// 	'order' => 'ASC'
							// );

							// $loop = new WP_Query( $args );
								
							// while ( $loop->have_posts() ) : $loop->the_post(); 

							// 	$photo_realisation = get_field( "photo" );
							// 	$lien_realisation = get_permalink();
							// 	$url_realisation = get_field( "url" );
								
							// 	echo '<div class="realisations-detail col-md-4 col-sm-6 col-6">';
							// 		echo '<div class="realisations-detail-image">';
							// 			echo '<img src="'.esc_url($photo_realisation['url']).'" alt="'.esc_attr($photo_realisation['alt']).'" draggable="false"/>';
							// 		echo '</div>';
							// 		// echo '<div class="realisations-detail-contenu">';
							// 		// 	echo '<p>'.the_title().'</p>';
							// 		// 	echo '<p>'.$description_realisation.'</p>';
							// 		// echo '</div>';
							// 		// echo '<div class="realisations-detail-liens">';
							// 		// 	echo '<a href="'.$lien_realisation.'">Plus d\'informations</a>';
							// 		// 	echo '<a href="'.esc_url($url_realisation['url']).'">Voir le site</a>';
							// 		// echo '</div>';
							// 	echo '</div>';

							// endwhile;

							// wp_reset_postdata();

						?>

						<div class="global-btn-jaune col-md-12">
							<a href="/contact" class="btn-jaune">Me contacter</a>
						</div>
						
					</div>
				</div>
			</div> -->

			<!-- Me contacter -->
			<div class="contact-home-wrapper">
				<div class="container">
					<div class="contact-home row">

						<div class="contact-home-content col-lg-6 col-md-12">

							<?php

								$titre_contact = get_field( "contact_titre" );
								$description_contact = get_field( "contact_description" );
								
								echo '<div class="contact-home-titre">';
									echo '<h2 class="titre-design">'.$titre_contact.'</h2>';
									echo $description_contact;
								echo '</div>';

							?>

							<div class="global-btn-jaune">
								<a href="/contact" class="btn-jaune">Me contacter</a>
							</div>

						</div>
						
						<div class="contact-home-rs col-lg-6 col-md-12">

							<?php

								$titre_contact_reseaux = get_field( "contact_reseaux" );
								$description_contact_reseaux = get_field( "contact_reseaux_description" );
								
								echo '<div class="contact-home-titre">';
									echo '<h2 class="titre-design">'.$titre_contact_reseaux.'</h2>';
									echo $description_contact_reseaux;
								echo '</div>';

							?>

							<a href="https://www.malt.fr/profile/raphaeldupre">
								<img src="https://www.raphael-dupre.fr/wp-content/themes/raphael-dupre/img/malt.png" alt="Malt" draggable="false">
							</a>
							<a href="https://fr.linkedin.com/in/rapha%C3%ABl-dupr%C3%A9-42859bb1" target="_blank">
								<img src="https://www.raphael-dupre.fr/wp-content/themes/raphael-dupre/img/linkedin.png" alt="Linkedin" draggable="false">
							</a>
						</div>

					</div>
				</div>
			</div>

		</div><!-- #main -->
	</section><!-- #primary -->

<?php

get_footer();

?>