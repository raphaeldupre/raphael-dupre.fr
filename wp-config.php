<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache


/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'raphaeldcgprod' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'raphaeldcgprod' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '678DX2VEyED6u8xLydPyw6iqt96BQ5' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'raphaeldcgprod.mysql.db' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Y gLr7&3{~!Mc/mt2Ivk74.S`fbvC/]T8w{qJr)-SyGxP6%Z`T[3<o#SpF;2M8t,' );
define( 'SECURE_AUTH_KEY',  '#X/=|cow59egy0QK}uA.Hs]v(>BL<5ClThrx}k0&+:ljvyf#9s#p33nuA =p]+D:' );
define( 'LOGGED_IN_KEY',    'MmYY [vIrvw{/!:Qvgi==O$<HD2F+^{r%C|@KQN_MbsxV>;(VjhBWewlahy}{c`K' );
define( 'NONCE_KEY',        'fL1m5@/}2k?uLyE0.$DNo,(h)YJMX[I|AJtN`ssEe,6-wb*3b##G0->hr,#1`Emw' );
define( 'AUTH_SALT',        'L4}TuqzV&!xCGco=++4o::Yfj}*og Ps@Y@AP1IrUO&?s|$<U0pHwQ/f#D,/j]P`' );
define( 'SECURE_AUTH_SALT', 'S[St F;o A[nC=YzKFf{NAV8un$/q6tfJm<Aouhs{,(~L=D%X?+Y>~D*$M<6ePXb' );
define( 'LOGGED_IN_SALT',   'MU&[qsI60a]oDJ`2!vpDCY/0&^#;AJ+U!<a+wJPiXzyUOi.qkF;ej{iKc,@&6f0|' );
define( 'NONCE_SALT',       'TefJl:Rxy C}y!lp^bDta!Png{.X.ZPGX.UFeHz$<Je[O`,t<iB?iXXlY|ug=?Y)' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
